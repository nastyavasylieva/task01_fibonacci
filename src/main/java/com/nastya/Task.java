/**
 * Copyright (c) Epam, Inc.
 */
package com.nastya;

import java.util.Scanner;

/**
 * Program for home task #1.
 *
 * @author Anastasiia Vasylieva
 * @version 1.0 30 Mar 2019
 */
public class Task {

    /**Variable that contains max odd element.*/
    private static int maxOddElement;
    /**Variable that contains max even element.*/
    private static int maxEvenElement;
    /**Variable that contains count of percents.*/
    private static final int PERCENT = 100;

    /**Default constructor.*/
    protected Task() {

    }
    /**
     * @param args Parameters from command line.
     */
    public static void main(final String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Task 1");
        System.out.println("Enter interval (for example: 1 100)");
        int beginInterval = scanner.nextInt();
        int endInterval = scanner.nextInt();
        doTask1(beginInterval, endInterval);

        System.out.println("Task 2");
        System.out.println("Enter count of Fibonacci numbers");
        int fibonacciNumbersCount = scanner.nextInt();
        doTask2(fibonacciNumbersCount);

    }

    /**
     * Prints odd elements in current interval from start to end
     * and even elements from end to start.
     * Prints sum of odd and even elements in current interval.
     *
     * @param beginInterval first number of the interval.
     * @param endInterval   last number of the interval.
     */
    public static void doTask1(final int beginInterval, final int endInterval) {
        int sumOfOddElements = 0;
        int sumOfEvenElements = 0;

        System.out.println("Odd elements:");
        for (int i = beginInterval; i <= endInterval; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
                sumOfOddElements += i;
                maxOddElement = i;
            }
        }

        System.out.println("Even elements:");
        for (int i = endInterval; i >= beginInterval; i--) {
            if (i % 2 == 1) {
                System.out.println(i);
                sumOfEvenElements += i;
            }
        }
        if (endInterval % 2 == 1) {
            maxEvenElement = endInterval;
        } else {
            maxEvenElement = endInterval - 1;
        }

        System.out.println("Sum of odd elements = " + sumOfOddElements);
        System.out.println("Sum of even elements = " + sumOfEvenElements);
        System.out.println(maxEvenElement + " " + maxOddElement);

    }

    /**
     * Prints Fibonacci sequence and percentage of odd
     * and even numbers of the sequence.
     *
     * @param fibonacciNumbersCount count of Fibonacci numbers.
     */
    public static void doTask2(final int fibonacciNumbersCount) {
        int oddElementsCount = 0;
        int evenElementsCount = 0;
        int number1 = 0;
        int number2 = 1;
        if (maxEvenElement < maxOddElement) {
            number1 = maxEvenElement;
            number2 = maxOddElement;
        } else {
            number2 = maxEvenElement;
            number1 = maxOddElement;
        }
        System.out.println("Fibonacci sequence:");
        System.out.println(number1);
        System.out.println(number2);

        for (int i = 0; i < fibonacciNumbersCount - 2; i++) {
            int number3 = number1 + number2;
            if (number3 % 2 == 0) {
                oddElementsCount++;
            } else {
                evenElementsCount++;
            }
            System.out.println(number3);
            number1 = number2;
            number2 = number3;
        }
        double percentageOfOddElements = (oddElementsCount * PERCENT)
                / fibonacciNumbersCount;
        double percentageOfEvenElements = (evenElementsCount * PERCENT)
                / fibonacciNumbersCount;
        System.out.println("Percentage of odd elements = "
                + percentageOfOddElements + "%");
        System.out.println("Percentage of even elements = "
                + percentageOfEvenElements + "%");

    }

}
